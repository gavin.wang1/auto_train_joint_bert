## Folder

```
./BERT/model
    |- joint_bert
        |- bert_wwm  # wwm模型
        |- run_kfold_bert.sh  # start from here

    |- Data
        |- Task
            |-source.csv # source data
        |- Task2
            |-source.csv # source data
        .
        .
        .
```

## run

```
1. sh run.sh # run docker
2. export Task_label=<Task> # data folder name
3. sh run_kfold_bert.sh
```

## output

* ./BERT/model/Data/.../output/DataFolder/test_results.tsv
* ./BERT/model/Data/.../output/DataFolder/test_results.txt
* ./BERT/model/Data/.../output/DataFolder/best_class_threshold.txt



# 訓練模型
1. 下載模型
	wwm模型下載：
        https://drive.google.com/file/d/1RoTQsXp2hkQ1gSRVylRIJfQxJUgkfJMW/view

2. 取得訓練資料csv檔案，做K-Flod

	訓練資料目前由Gavin產生multi_chinese_and_ner.csv。
	放到資料夾中執行 KFOLD.ipynb，會在data資料夾下產生5份訓練資料

3. 訓練模型

	python ./joint_bert.py \
  	--task_name=customized \
  	--do_train=true \
  	--do_eval=true \
  	--do_predict=true \
  	--data_dir=./data/data0 \
  	--vocab_file=./bert_wwm/vocab.txt \
  	--bert_config_file=./bert_wwm/bert_config.json \
  	--init_checkpoint=./bert_wwm/bert_model.ckpt \
  	--max_seq_length=64 \
  	--train_batch_size=16 \
  	--learning_rate=5e-5 \
  	--num_train_epochs=5 \
  	--output_dir=./output/ \
  	--alpha=0.1

	data_dir指定訓練資料的位置 \
	vocab_file, bert_config_file, init_checkpoint須指到預訓練模型的資料夾下。 \
	output_dir輸出資料夾位置。重新訓練的話，記得要先清空舊檔案（output資料夾）\
	alpha: loss = alpha*slot_loss + (1-alpha)*intent_loss，alpha 越大, 越重視slot loss。反之越重視intent_loss \

4. 檔案輸出

	輸出資料夾下 \
	label_map: index與intent標籤的mapping \
	tags_map: index與slot標籤的mapping \
        eval_results.txt: dev set的各項metrics結果 \
	test_results.txt: test set的各項metrics結果 \
	best_class_threshold.txt: 在dev set下找到個類別f1最佳的threshold，後續會用在test set的預測上。 \
	test_results.tsv: test set的標籤與模型輸出結果。frame_O_X表示是否完全預測正確，slot與intent皆全對。intent_OX表示intent是否預測正確。slot_OX表示slot是否預測正確。 \