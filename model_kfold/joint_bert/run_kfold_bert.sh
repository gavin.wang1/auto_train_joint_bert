#!/usr/bin/env bash

#export Task_label=Task
python3 kfold.py $Task_label

for data in data0 data1 data2 data3 data4
do
    python3 joint_bert.py \
      --task_name=customized \
      --do_train=true \
      --do_eval=true \
      --do_predict=true \
      --do_export=true \
      --data_dir=../Data/$Task_label/train/${data} \
      --vocab_file=./bert_wwm/vocab.txt \
      --bert_config_file=./bert_wwm/bert_config.json \
      --init_checkpoint=./bert_wwm/bert_model.ckpt \
      --max_seq_length=64 \
      --train_batch_size=16 \
      --learning_rate=5e-5 \
      --num_train_epochs=1 \
      --output_dir=../Data/$Task_label/output/${data} \
      --export_dir=../Data/$Task_label/output/${data} \
      --alpha=0.1
done