

python3 joint_bert.py \
  --task_name=customized \
  --do_train=true \
  --do_eval=true \
  --do_predict=true \
  --data_dir=../Data/2019-08-19/train/data0 \
  --vocab_file=./bert_wwm/vocab.txt \
  --bert_config_file=./bert_wwm/bert_config.json \
  --init_checkpoint=./bert_wwm/bert_model.ckpt \
  --max_seq_length=64 \
  --train_batch_size=16 \
  --learning_rate=5e-5 \
  --num_train_epochs=5 \
  --output_dir=../Data/2019-08-19/output/ \
  --alpha=0.1
